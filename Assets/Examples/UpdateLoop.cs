﻿using UnityEngine;
using UnityAsync;

// This example shows how async coroutines can immitate an update loop.
public class UpdateLoop : AsyncBehaviour
{
	[SerializeField, Range(1, 100)]
	float speed = 50;

	void Start()
	{
		RotateIndefinitely();
	}

	async void RotateIndefinitely()
	{
		while(true)
		{
			await NextUpdate();
			transform.Rotate(0, Time.deltaTime * speed, 0);
		}
	}
}