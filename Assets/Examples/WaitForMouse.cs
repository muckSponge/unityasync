﻿using UnityEngine;
using UnityAsync;
using System.Threading.Tasks;

// This example shows how Tasks can be used as coroutines.
public class WaitForMouse : AsyncBehaviour
{
	async void Start()
	{
		await WaitForMouseDown();

		Debug.Log("You just clicked, I'm outta here!");

		Destroy(gameObject);
	}

	async Task WaitForMouseDown()
	{
		while(true)
		{
			await NextUpdate();

			if(Input.GetMouseButtonDown(0))
				break;
		}
	}
}